#!/bin/bash

# 起始年份和月份
START_YEAR=2024
START_MONTH=1

# 当前年份和月份
END_YEAR=$(date +%Y)
END_MONTH=$(date +%m | sed 's/^0*//')

BASEDIR=C:\\Users\\mlamp\\git\\ssp_source
cd $BASEDIR

# 获取项目中的所有开发人员
developers=$(git log --format='%aN' | sort -u)

# 遍历每个月
for (( year=$START_YEAR; year<=$END_YEAR; year++ ))
do
  for (( month=1; month<=12; month++ ))
  do
    if (( year == END_YEAR && month > END_MONTH)); then
      break
    fi
    
    # 格式化月份
    formatted_month=$(printf "%02d" $month)

    # # 输出当前统计月份
    # echo "### $year-$formatted_month ###"

    # 遍历每个开发人员
    # for dev in $developers
    # do
    #   echo "Developer: $dev"

    #   # 统计该开发人员在当前月份的新增和删除行数
    #   git log --author="$dev" --since="$year-$formatted_month-01" --until="$year-$formatted_month-31" \
    #     --pretty=tformat: --numstat | \
    #     awk '{ added += $1; deleted += $2 } END { printf "Added lines: %s, Deleted lines: %s\n", added, deleted }'
        
    # done
    # echo ""
    git log --since="$year-$formatted_month-01" --before="$year-$formatted_month-31" --pretty='%aN' | sort -u | while read name; do echo -en "$name,$formatted_month月,"; git log --author="$name" --since="$year-$formatted_month-01" --before="$year-$formatted_month-31" --pretty=tformat: --numstat | awk '{ add += $1; subs += $2; loc += $1 + $2 } END { printf "added lines: %s, removed lines: %s, total lines: %s \n", add, subs, loc }' -; done
  done
done