package com.mamingchao.issue.hash;

/**
 * 自定义一个位图
 * Created by mlamp on 2024/11/4.
 */
public class BitMap {

    // 一个int 4字节；10个int 就是40个字节，320位的bitmap
    int[] bitMap = new int[10];
    // 因为是int 类型，所以是 32位
    int m = 32;

    /**
     * 如果bitmap长度是320位，那么n不要超过320
     * @param n 指定位图的位置
     * @return 返回位图上指定n位置的状态；0或者1
     */
    private int getAppointPositionStatus(int n) {
        // 边界值判断
        if (n > 320){
            return -1;
        }
        // 位置在 第几个 int上
        int numIndex = n/m;
        // 在numIndex位置 int的第几位上
        int bitIndex = n%m;

        int s = 1 & (bitMap[numIndex] >> bitIndex);
        return s;
    }

    /**
     * 如果bitmap长度是320位，那么n不要超过320;
     * 将指定位置的值，设置为1
     * @param n 指定位图的位置
     */
    private void setAppointPositionTrue(int n) {
        // 位置在 第几个 int上
        int numIndex = n/m;
        // 在numIndex位置 int的第几位上
        int bitIndex = n%m;

        bitMap[numIndex] = bitMap[numIndex]  | (1 << bitIndex);
    }

    /**
     * 如果bitmap长度是320位，那么n不要超过320;
     * 将指定位置的值，设置为0
     * @param n 指定位图的位置
     */
    private void setAppointPositionFalse(int n) {
        // 位置在 第几个 int上
        int numIndex = n/m;
        // 在numIndex位置 int的第几位上
        int bitIndex = n%m;

        bitMap[numIndex] = bitMap[numIndex]  & (~(1 << bitIndex));
    }


//    位图
//    int numIndex = n/m;  178/32
//    int bitIndex = n/m;  178%32
//
//    // 拿到178位的状态
//    int s= 1 & (arr[numIndex] >> bitIndex)
//
//    // 把178位的状态改成0
//    arr[numIndex] =arr[numIndex]  & (~(1 << bitIndex))
//
//    // 把178位的状态改成1
//    arr[numIndex] = arr[numIndex]  | (~(1 << bitIndex))


}
