package com.mamingchao.issue.graph;

/**
 * Created by mlamp on 2024/7/22.
 */
public class GraphEdge {
    //边的权重
    private int weight;
    // 边的起始节点
    private GraphNode from;
    // 边的终止节点
    private GraphNode to;

    public GraphEdge(int w, GraphNode f, GraphNode t){
        this.weight = w;
        from = f;
        to = t;
    }

    public int getWeight() {
        return weight;
    }

    public GraphNode getFrom() {
        return from;
    }

    public GraphNode getTo() {
        return to;
    }
}
