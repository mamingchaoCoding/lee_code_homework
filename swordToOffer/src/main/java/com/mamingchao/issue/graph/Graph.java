package com.mamingchao.issue.graph;

import java.util.HashMap;
import java.util.HashSet;

/**
 * ??????
 * Created by mlamp on 2024/7/22.
 */
public class Graph {

    // node 唯一编号（类似于身份证） 和 node对象的映射
    private HashMap<Integer, GraphNode> graphNodes;

    // 图里所有的边
    private HashSet<GraphEdge> graphEdges;

    public Graph(){
        graphNodes = new HashMap<>();
        graphEdges = new HashSet<>();
    }

    public HashMap<Integer, GraphNode> getGraphNodes() {
        return graphNodes;
    }

    public HashSet<GraphEdge> getGraphEdges() {
        return graphEdges;
    }
}
