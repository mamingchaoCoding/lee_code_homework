package com.mamingchao.issue.graph;

import java.util.HashSet;

/**
 * Created by mlamp on 2024/7/22.
 */
public class GraphNode {

    // 图节点的值，假设是唯一的
    private int value;
    // 指进来的边，有几条  入度
    private int in;
    // 伸出去的边，有几条  出度
    private int out;

    private HashSet<GraphNode> myNexts;
    private HashSet<GraphEdge> myEdge;

    public GraphNode(int value){
        this.value = value;
        in = 0;
        out = 0;
        myNexts = new HashSet<>();
        myEdge = new HashSet<>();
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getIn() {
        return in;
    }

    public void setIn(int in) {
        this.in = in;
    }

    public int getOut() {
        return out;
    }

    public void setOut(int out) {
        this.out = out;
    }

    public HashSet<GraphNode> getMyNexts() {
        return myNexts;
    }

    public void setMyNexts(HashSet<GraphNode> myNexts) {
        this.myNexts = myNexts;
    }

    public HashSet<GraphEdge> getMyEdge() {
        return myEdge;
    }

    public void setMyEdge(HashSet<GraphEdge> myEdge) {
        this.myEdge = myEdge;
    }
}
