package com.mamingchao.issue.binaryTree;

/**
 * 对应case 5
 * 5、在二叉树中找到一个节点的后继节点
 【题目】现在有一种新的二叉树节点类型如下:
 public class Node {
 public int value;
 public Node left;
 public Node right;
 public Node parent;
 public Node(int val){
 value=val;
 }
 }
 该结构比普通二叉树节点结构多了一个指向父节点的parent指针。
 假设有一棵Node类型的节点组成的二叉树,树中每个节点的parent指针都正确地指向自己的父节点,头节
 点的parent指向null。
 只给一个在二叉树中的某个节点node,请实现返回node的后继节点的函数。
 在二叉树的中序遍历的序列中,node的下一个节点叫作node的后继节点。
 * Created by mlamp on 2024/7/24.
 */
public class InOrderTreeNextNode {
}
