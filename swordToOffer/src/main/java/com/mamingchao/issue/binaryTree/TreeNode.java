package com.mamingchao.issue.binaryTree;

/**
 * 二叉树节点 对象
 * Created by mamingchao on 2024/7/23.
 */
public class TreeNode {

    // 二叉树节点的 key
    private int key;

    // 二叉树节点的 值
    private String value;

    // 二叉树节点的 自定义属性
    private int attribute;
    // 二叉树节点的 左孩子
    public TreeNode leftChild;
    // 二叉树节点的 右孩子
    public TreeNode rightChild;

    public TreeNode(int k, String v){
        this.key = k;
        this.value = v;
        leftChild = null;
        rightChild = null;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // 帮助我生产该类上面没有getter和setter的字段的 getter和setter方法
    public int getKey() {
        return key;
    }
    
    public void setKey(int key) {
        this.key = key;
    }
    
    public int getAttribute() {
        return attribute;
    }
    
    public void setAttribute(int attribute) {
        this.attribute = attribute;
    }

}
