package com.mamingchao.issue.UnionFindSet;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mamingchao on 2022/8/25.
 */
public class CaculateSamePersonNum {
    static class User {
        String a;
        String b;
        String c;

        public User(String a, String b, String c){
            this.a = a;
            this.b = b;
            this.c = c;
        }

         @Override
        public int hashCode(){
            int prime = 31;
            int result = 1;

            result = prime * result + a == null ? 0 : a.hashCode();
            result = prime * result + b == null ? 0 : b.hashCode();
            result = prime * result + c == null ? 0 : c.hashCode();

            return result;
        }

        public boolean equals(Object object) {
            if (object == null)
                return false;

            if (object == this)
                return true;
            if (object instanceof User) {
                User user = (User)object;
                if (user !=null) {
                    if (a.equalsIgnoreCase(user.a) || b.equalsIgnoreCase(user.b) || c.equalsIgnoreCase(user.c)) {
                        return true;
                    }
                }
            }

            return false;

        }
    }

    public static void main(String[] args) {
        // 比如user1{10,20,30}, user2{5,20,1000},user3{5,1,200}
        User user1 = new User("10","20","30");
        User user2 = new User("5","20","1000");
        User user3 = new User("5","1","2000");

        Set<User> result = new HashSet<>();
        result.add(user1);
        result.add(user2);
        result.add(user3);

        System.out.printf("人数 %d", result.size());
    }
}
